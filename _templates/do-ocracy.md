---
layout: rule
title: Do-ocracy
permalink: /templates/do-ocracy/
icon: /assets/tabler_icons/lock-open.svg

community-name: 

# BASICS
structure: Those who take initiative to do work for the community can decide how they do that work. If any community members hold serious concerns, they can start a proposal to halt what someone is doing.
mission:
values: This community shares a bias for action and a commitment to empowering and encouraging its members. Exceptions should be on the basis of serious ethical or moral concerns, not disagreements. Do-ers should keep in mind the best interests of the community as a whole and consult with community members when those interests are not clear.
legal: 

# PARTICIPANTS
membership: A member is anyone who has actively contributed to the community's mission within the past year.
removal: If a member believes another member is not respecting the serious concerns of others, a proposal for removal may be submitted. The proposal goes for a vote before all members for 1 week. If at least 2/3 of voting members agree with the proposal, the offending member is removed.
roles:
limits: If a member exhibits disregard for others' serious concerns, they can be removed from the community.

# POLICY
rights: Members have a right to decide how they make their contributions, while other members have the right to object on the basis of serious concerns.
decision: Individual members and subgroups can decide what and how they want to contribute to the community, reasonably respecting the community's precedent and the expressed opinions of others.
implementation: Community members are responsible for carrying out their own initiatives, and they will not expect any other members to do so, except by those members' own volition.
oversight: If another member holds serious ethical or moral concerns, that member can create a proposal stating clearly what actions should be prohibited. The proposal goes for a vote before all members for 1 week. If at least 2/3 of voting members agree with the proposal, it passes and becomes a policy for the community.

# PROCESS
access: 
economics: 
deliberation: 
grievances: 

# EVOLUTION
records: 
modification: A proposal to change this Rule goes for a vote before all members for 1 week. If at least 2/3 of voting members agree with the proposal, it passes and the Rule changes accordingly.
---

Use this template as-is or edit it to adapt it to your community.

Blends [do-ocracy](https://medlabboulder.gitlab.io/democraticmediums/mediums/do-ocracy/) and [lazy consensus](https://medlabboulder.gitlab.io/democraticmediums/mediums/lazy_consensus/).
