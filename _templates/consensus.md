---
layout: rule
title: Consensus
permalink: /templates/consensus/
icon: /assets/tabler_icons/arrows-minimize.svg

community-name: 

# BASICS
structure: Decisions that affect the group collectively must be agreed on by all members.
mission:
values: The consensus process emphasizes listening, empathy, creativity, and a refusal to allow a majority to overrun a minority. Although group decisions must be made together, the process also honors participants' autonomy in all other matters.
legal: 

# PARTICIPANTS
membership: New participants may join as long as no existing participants object.
removal: Participants may be removed if a proposal to do so passes the consensus process of all other participants.
roles: The group can create roles, attach participants to them, and assign them authority over specified sub-domains.
limits: 

# POLICY
rights: 
decision: The consensus process proceeds as follows. Any participant may make a proposal for a decision at any group meeting open to which all participants are invited. Once stated, the proposal may be discussed, and the proposer may accept others' amendments. The proposal passes if all participants present either move to approve it or abstain from doing so. If a participant holds a serious concern—one that they believe threatens the basic values or integrity of the group—they may block it, and it does not pass.
implementation: Unless otherwise stated, the proposer is responsible for leading the implementation of their proposal.
oversight: 

# PROCESS
access:
economics: 
deliberation: 
grievances: 

# EVOLUTION
records: 
modification: The group may modify this Rule through the consensus process.
---

Use this template as-is or edit it to adapt it to your community.

Inspired by [Quaker decision making](https://www.afsc.org/testimonies/decision-making) and [anarchist traditions](https://theanarchistlibrary.org/library/seeds-for-change-consensus-decision-making).
