---
layout: module
title: Petition
permalink: /modules/petition/
summary: If a certain number of people sign on to a statement, it triggers the official decision-making process.
type: process
---
