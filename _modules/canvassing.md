---
layout: module
title: Canvassing
permalink: /modules/canvassing/
summary: Peer-to-peer campaigning to achieve popular support.
type: process
---

Canvassing is an activity performed by individuals wishing to reach others for a campaign or cause. Election cycles, grassroots movements and fundraising initiatives are just a few examples of situations in which canvassing might be implemented. By knocking on doors and speaking with individuals, canvassers hope to influence undecided voters for the candidate they are representing, raise awareness about an issue, and increase civic engagement.

**Input:** team of representatives, informational materials, information on geographic areas to canvas & their demographics

**Output:** persuading voters to act in specific ways and support particular causes, increasing political engagement, increasing face-to-face interaction between candidates, representatives, and voters

## Background

Canvassing as a strategy to reach potential supporters has a long history. In the Roman Republic, candidates would introduce themselves to individuals at the Forum and attempt to gain their political support. In Elizabethan-era England, candidates sought to determine whether they had enough votes to win before announcing interest in a Parliament seat. Canvassing thus served as an indicator of whether or not a candidate had enough support to continue running; if they did not secure enough votes, they would drop out. 

In the United States, an extremely low voter turnout in 1996 led to studies on the effects of canvassing. Canvassing has become more widely practiced since the early 2000s.

## Feedback loops

### Sensitivities

* Increases political engagement
* Informs publics on particular issues
* Increases identification between voters and particular campaigns, candidates, and causes

### Oversights

* High cost due to small scope of individual reach and modes of persuasion
* Diverts agency of voters from learning information for themselves and instead relies on canvassers for targeted information
* Studies on effectiveness yield contradictory results
* Can result in discriminatory targeting of particular communities
* Has a history of corruption and bribery to gain votes

## Implementations

### Communities

Canvassing is used in societies around the world where there is some form of participatory governance

### Tools

* A variety of canvassing apps enable canvassers to use mobile devices to better target and organize their efforts
* Databases with voter information inform geographic areas chosen by canvassers

## Further resources

* Green, D. P., Gerber, A. S. and Nickerson, D. W. (2003), Getting Out the Vote in Local Elections: Results from Six Door‐to‐Door Canvassing Experiments. Journal of Politics, 65: 1083-1096. doi:10.1111/1468-2508.t01-1-00126
* [Lobbying](lobbying.md) is a more formal, structured form of direct persuasion within political institutions
* Nielsen, Rasmus Kleis (5 February 2012). Ground Wars: Personalized Communication in Political Campaigns. Princeton University Press. ISBN 978-0-691-15305-6.
