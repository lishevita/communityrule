---
layout: module
title: Eloquence
permalink: /modules/eloquence/
summary: An opportunity to persuade others with rhetorical effect.
type: culture
---

Eloquence is a graceful and artistic form of expression, associated strongly with oratory and public speaking. Unlike rhetoric, it does not aim to argue for a specific view based upon the three modes of persuasion—though an argumentative speech may indeed be eloquent, it is not fundamental to its meaning. Eloquence is described as the force of a certain delivery based on the manner of speaking or writing employed by the orator or author; it flows seamless and demonstrates a command of language.

**Input:** comfort with language and communication; platform for speaking and writing; an intended audience

**Output:** speech or piece of writing with appeal through the artistry of the chosen language and emotion it provokes

## Background

Notions of eloquence date back to ancient times. In Greek mythology, Calliope is a beautiful-voice muse of eloquence and epic poetry tutored by Apollo. 
Cicero, a Roman statesman widely considered one of the greatest speakers of all time, writes about the necessity for style, flow, and conveying emotions in his dialogue “De Oratore” (55 BC). He finds eloquence one of the most powerful aspects of oratory, separate from the argumentative goals of rhetoric. Petrarch, a poet of the Italian Renaissance, taught eloquence as the ultimate goal in mastering language and communication.

## Feedback loops

### Sensitivities

* Eloquence is connoted with politeness and morality, something that further separates it from rhetoric, which is often framed as unethical and manipulative
* Provides the vehicle through which to deliver important logical and convincing content
* Builds credibility for the speaker or author; aims to positively impact audience

### Oversights

* Eloquence can divert audiences from content and mask it in appealing yet superficial language
* Can discourage audiences from thinking critically about important issues, as eloquent language may leave no room for contention 
* The entirety of an individual’s qualifications may be undermined and render them poorly suited for the job in the eyes of the audience if they lack eloquence, despite their ability and aptitude in practice.

## Implementations

### Communities

Eloquence is frequently associated with elections and political speeches. Known for their eloquence is a host of figures, political and otherwise, from Roman General Mark Antony to Martin Luther King Jr., from Barack Obama to Winston Churchill, from Margaret Thatcher to Oprah Winfrey.

### Tools

* [“The Art of Public Speaking”]( https://www.learnoutloud.com/Free-Audio-Video/Self-Development/Public-Speaking/The-Art-of-Public-Speaking/47818) by Dale Carnegie, available in audiobook
* Ginger Public Speaking offers [free and low-cost courses]( https://www.gingerpublicspeaking.com/courses/free-online-courses-classes) in public speaking
* [The Public Speaking Project]( http://www.publicspeakingproject.org/) offers online tools in print and module format

## Further resources

O’Connell, D. C., & Kowal, S. (2002). Political eloquence. In The social psychology of politics (pp. 89-103). Springer, Boston, MA.

Donoghue, D. (2008). On Eloquence. Yale University Press.

Elbow, P. (2011;2012;). Vernacular eloquence: What speech can bring to writing. New York;Oxford;: Oxford University Press. doi:10.1093/acprof:osobl/9780199782505.001.0001

Gildenhard, I. (2011). Creative eloquence: The construction of reality in Cicero's speeches. Oxford: Oxford University Press
