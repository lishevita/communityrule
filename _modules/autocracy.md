---
layout: module
title: Autocracy
permalink: /modules/autocracy/
summary: One person holds sole decision-making authority over a certain domain.
type: structure
---
