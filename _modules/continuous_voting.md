---
layout: module
title: Continuous voting
permalink: /modules/continuous_voting/
summary: Voting can occur anytime, not just on a fixed schedule, and decisions can change if participants alter their votes.
type: decision
---

# Continuous voting

<!---https://medium.com/giveth/conviction-voting-34019bd17b10-->

Continuous voting is a system in which the vote occurs on a continuous or frequent basis, rather than during discrete election periods.

**Input:** Ballot, randomized or universal electorate, easy mechanism for vote changing

**Output:** Frequent or real-time results

## Background

Continuous voting mechanisms have been proposed in a variety of forms and contexts, though largely only since the advent of Internet technology that would streamline vote-changing and counting. [Delegation](delegation.md) systems like "liquid democracy" generally use continuous voting.

Economics blogger Steve Randy Waldman [proposed](https://www.interfluidity.com/v2/7043.html) in 2018 a model by which random subsets of the electorate would be polled on their representatives at frequent intervals. The same year, BlockScience CEO Michael Zargham [proposed](https://github.com/BlockScience/conviction/blob/master/social-sensorfusion.pdf) a model called "conviction voting," in which votes grow stronger the longer the remain with a particular choice, and greater stake in the system increases vote strength, among other properties. It has been [adopted](https://medium.com/giveth/conviction-voting-34019bd17b10) as part of the blockchain-based Commons Stack project.

## Feedback loops

### Sensitivities

* Provides feedback closer to real-time
* Reduces vulnerability to timed attacks on democratic deliberation

### Oversights

* No fixed terms of elected cohorts, reducing ability to plan for a session in office
* Can instill greater insecurity among elected representatives

## Implementations

### Communities

* [Commons Stack](https://commons-stack.gitbook.io/wiki/) (in development)
* Various pirate parties using liquid democracy

### Tools

* [Democracy Earth](https://democracy.earth) implements liquid democracy

## Further resources

* [Delegation](delegation.md)
* Kay. May 29, 2019. "[Conviction Voting: From ad-hoc voting to continuous voting](https://medium.com/giveth/conviction-voting-34019bd17b10)." Giveth blog.
