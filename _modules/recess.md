---
layout: module
title: Recess
permalink: /modules/recess/
summary: An official break in governance might calm conflicts and promote accountability to constituents.
type: process
---
