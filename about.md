---
layout: rule
title: About
permalink: /about/

community-name: 

# BASICS
structure: CommunityRule is a governance toolkit for great communities.
mission: Too many of our communities adopt default governance practices that rely on the unchecked authority of founders, admins, or moderators, lacking even basic features of small-scale democracy. The purpose of CommunityRule is to help communities establish appropriate norms for decision-making, stewardship, and culture.
values:
legal: We acknowledge that the governance practices we share here have long roots in diverse cultures. To ensure this inheritance remains a commons, content on CommunityRule is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License, which means that anyone has the right to use and adapt it, and adaptations must be re-shared under the same license.

# PARTICIPANTS
membership: CommunityRule is a project of the Media Enterprise Design Lab at the University of Colorado Boulder, in collaboration with the Metagovernance Project (metagov.org). Anyone who finds this project useful, or potentially so, is welcome to contribute suggestions, changes, and additions.
removal:
roles: CommunityRule is currently administered and primarily authored by MEDLab director Nathan Schneider.
limits: 

# POLICIES
rights: 
decision: 
implementation: 
oversight: 

# PROCESSES
access: 
economics: 
deliberation: To contribute to the project, post Issues and Merge Requests to the project on GitLab (see footer for link), or send an email to medlab@colorado.edu.
grievances: 

# EVOLUTION
records: 
modification: If a community of contributors and users forms around CommunityRule, it should develop into a more community-driven governance model with the help of CommunityRule itself.
---

This page is itself a Rule. More background information is at the [FAQ]({% link _about/faq.md %}), and use the [Guides]({% link guides.md %}) to get started making a Rule of your own.
